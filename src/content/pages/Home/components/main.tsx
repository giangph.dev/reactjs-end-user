import MainContent from './mainContent';
import eofficeImge from "src/assets/images/eoffice.png";
import ehrmsImge from "src/assets/images/ehrms.png";
import elearningImge from "src/assets/images/elearning.png";
import edocsImge from "src/assets/images/edocs.png";
const Main = () => {
  const bannerData = [
    { name: "eOffice Solution", img: eofficeImge },
    { name: "eHrms Solution", img: ehrmsImge },
    { name: "eDocs Solution", img: edocsImge },
    { name: "eLearning Solution", img: elearningImge },
  ];
  return (
    <>
      <MainContent bannerData={bannerData} />
    </>
  )
};

export default Main;