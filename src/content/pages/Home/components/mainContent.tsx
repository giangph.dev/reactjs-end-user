import { forwardRef, FC } from "react";
import CanvasContainer from "src/components/Canvas";
import { NavLink } from "react-router-dom";
interface EnhancedComponentProps {
  bannerData
};

const MainContent: FC<EnhancedComponentProps> = forwardRef((props,ref): JSX.Element => {
  const {bannerData} = props;
  return (
    <>
      <div className="position-relative">
        <CanvasContainer />
        <div className="bannerImage"></div>
        <div className="container position-absolute banner-content">
          <div className="d-flex w-100 align-items-center justify-content-between">
            {bannerData.map((item, key) => {
              return (
                <div className="text-center banner-content-items" key={key}>
                  <NavLink
                    className="font-weight-bold text-light d-block mb-3 banner-content-text"
                    to="/"
                  >
                    {item.name}
                  </NavLink>
                  <img
                    src={item.img}
                    alt=""
                    width={`${key === 3 ? "300px" : "200px"}`}
                  />
                </div>
              );
            })}
          </div>
        </div>
        <div className="position-absolute copyright"></div>
      </div>
    </>
  );
});

export default MainContent;