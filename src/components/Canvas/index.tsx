import { useState, useEffect, useRef } from "react";

function CanvasContainer() {
  const [isElementRendered, setIsElementRendered] = useState(false);
  var canvasE = useRef(null);
  var canvas = canvasE.current;
  var options = {
    star:{
      width:2,
      color:"rgba(255,255,0,.6)",
    },
    line:{
      color:"rgba(255,255,255,.4)",
      width: .1
    },
    radius: 168,
    distance: 186,
    length: 68,
    position: {
      x: (canvas ? canvas.offsetWidth : 0) * 1,
      y: (canvas ? canvas.offsetHeight: 0) * .5
    },
    width: window.innerWidth,
    height: window.innerHeight,
    velocity: .4,
    stars: []
  };
  var context = canvas ? canvas.getContext('2d') : undefined;
  function Star() {
    this.x = Math.random() * canvas.offsetWidth;
    this.y = Math.random() * canvas.offsetHeight;
    this.vx = options.velocity - (Math.random() * .5);
    this.vy = options.velocity - (Math.random() * .5);
    this.radius = Math.random() * options.star.width * devicePixelRatio;
  };
  Star.prototype = {
    create: function() {
      context.beginPath();
      context.arc(this.x, this.y, this.radius, 0, Math.PI * 2, false);
      context.fill();
    },
    animate: function() {
      var i;
      for (i = 0; i < options.length; i++) {
        var star = options.stars[i];
        if (star.y < 0 || star.y > canvas.offsetHeight) {
          star.vx = star.vx;
          star.vy = -star.vy;
        } else if (star.x < 0 || star.x > canvas.offsetWidth) {
          star.vx = -star.vx;
          star.vy = star.vy;
        }
        star.x += star.vx * devicePixelRatio;
        star.y += star.vy * devicePixelRatio;
      }
    },
    line: function() {
      var length = options.length, iStar, jStar, i, j;
      for (i = 0; i < length; i++) {
        for (j = 0; j < length; j++) {
          iStar = options.stars[i];
          jStar = options.stars[j];
          if ((iStar.x - jStar.x) < options.distance * devicePixelRatio && (iStar.y - jStar.y) < options.distance * devicePixelRatio && (iStar.x - jStar.x) > -options.distance * devicePixelRatio && (iStar.y - jStar.y) > -options.distance * devicePixelRatio) {
            if ((iStar.x - options.position.x) < options.radius && (iStar.y - options.position.y) < options.radius && (iStar.x - options.position.x) > -options.radius && (iStar.y - options.position.y) > -options.radius) {
              context.beginPath();
              context.moveTo(iStar.x, iStar.y);
              context.lineTo(jStar.x, jStar.y);
              context.stroke();
              context.closePath();
            }
          }
        }
      }
    }
  };
  const createStars = () => {
    var length = options.length, star, i;
    context.clearRect(0, 0, canvas.offsetWidth, canvas.offsetHeight);
    for (i = 0; i < length; i++) {
      options.stars.push(new Star());
      star = options.stars[i];
      star.create();
    };
    star.line();
    star.animate();
  };
  var setCanvas = () => {
    canvas.width = canvas.offsetWidth * devicePixelRatio;
    canvas.height = options.height * devicePixelRatio;
  };
  var setContext = () => {
    context.fillStyle = options.star.color;
    context.strokeStyle = options.line.color;
    context.lineWidth = options.line.width * devicePixelRatio;
  };
  var loop = (callback) => {
    callback();
    window.requestAnimationFrame(function() {
      loop(callback);
    });
  };
  var bind = () => {
    window.addEventListener("mousemove", function(e) {
      options.position.x = (e.pageX - canvas.offsetLeft) * devicePixelRatio;
      options.position.y = (e.pageY - canvas.offsetTop) * devicePixelRatio;
    });
  };
  var init = () => {
    setCanvas();
    setContext();
    loop(createStars);
    bind();
  };

  useEffect(() => {
    setIsElementRendered(true);
  }, []);

  useEffect(() => {
    if(isElementRendered) {
      init();
    }
  }, [isElementRendered]);

  return <canvas className="position-absolute" ref={canvasE} style={{
    width: "100%",
    height: "100%"
  }}></canvas>
}

export default CanvasContainer;
