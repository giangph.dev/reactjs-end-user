import { useRoutes } from 'react-router-dom';
import routes from 'src/router/router';
const App = (props) => {
  const content = useRoutes(routes);
  return <>
    {content}
  </>
}

export default App;
