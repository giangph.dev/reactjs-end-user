import { FC, ReactNode } from "react";
import Header from "./Header";
import { Outlet } from 'react-router-dom';
import Footer from "./Footer";

interface SidebarLayoutProps {
  children?: ReactNode;
}

const BaseLayout: FC<SidebarLayoutProps> = () => {
  return <>
    <Header />
    {/* <BannerContainer /> */}
    <div className="mainContent">
      <Outlet />
    </div>
    <Footer />
  </>
}

export default BaseLayout;
