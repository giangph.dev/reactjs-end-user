import MenuRightBtnAction from "./Actions";
import Menu from "./Menu";
import logo from "../../../assets/images/logo.png"
function Header() {
  return <>
    <div className="position-fixed w-100" id="headerTop">
      <nav className="navbar container rounded px-3">
        <div>
          <img src={logo} alt="Logo" width="70px" />
        </div>
        <Menu />
        <MenuRightBtnAction />
      </nav>
    </div>
    
  </>
}

export default Header;
