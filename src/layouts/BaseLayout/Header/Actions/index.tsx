import { t } from "i18next";
import { NavLink } from "react-router-dom";

const MenuRightBtnAction = () => {
  const array = [
    {path: "/login", name: "common.sign_in"},
    {path: "/register", name: "common.sign_up"}
  ]
  return <>
    <div className="topMenuAction d-flex aling-items-center">
      {array.map((item, key) => {
        return <div className="px-2" key={key}>
          <NavLink
            type="button"
            style={({isActive}) => ({
              background: isActive ? "#f09b24" : "#044564"
            })}
            className="btn btn-sm text-white border-0" 
            to={item.path}>{t(item.name)}</NavLink>
        </div>
      })}
    </div>
  </>
}

export default MenuRightBtnAction;