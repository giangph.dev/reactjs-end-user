import { items, itemsType } from './items';
import { NavLink } from 'react-router-dom';
import { t } from 'i18next';

const renderMenuItems = (menu: itemsType[]) => {
  if(!menu.length) return <></>;
  return <ul className="listMenu nav">
    {menu.map((item, index) => {
      return <li className="menuItem nav-item" key={index}>
        <NavLink 
          className="rounded py-2 px-0 px-lg-3 d-block nav-link"
          to={item.path}
          style={({isActive}) => ({
            color: isActive? "#f09b24": "#22b8d2"
          })}
        >{t(item.name)}</NavLink>
      </li>;
    })}
  </ul>
};

function Menu() {
  return <>
    {renderMenuItems(items)}
  </>
}

export default Menu;