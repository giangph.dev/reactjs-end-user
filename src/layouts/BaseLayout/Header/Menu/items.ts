export interface itemsType {
  id: number;
  name: string;
  child?: itemsType[];
  path: string;
};
const items:itemsType[] = [
  {
    id: 1,
    name: "common.home",
    path: "/home"
  },
  {
    id: 2,
    name: "common.about",
    path: "/about"
  },
  {
    id: 3,
    name: "common.product",
    path: "/product"
  },
  {
    id: 4,
    name: "common.contact",
    path: "/contact"
  },
];

export {
  items
};