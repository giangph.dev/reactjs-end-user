import { lazy } from "react";
import { Navigate } from "react-router";
import BaseLayout from "src/layouts/BaseLayout";

//Auth
const Login = lazy(() => import("src/content/pages/Login"));
const Home = lazy(() => import("src/content/pages/Home"));

const routes = [
  {
    path: "/",
    element: <BaseLayout />,
    children: [
      {
        path: "",
        element: <Navigate to={"/home"} />
      },
      {
        path: "*/",
        element: <Navigate to={"/home"} />
      },
      {
        path: "/login",
        element: <Login />
      },
      {
        path: "home",
        element: <Home />
      }
    ]
  }
];

export default routes;
